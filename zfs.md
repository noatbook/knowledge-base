### Enable / disable samba share

It seems ZFS uses some kind of usershare to do this sharing since no configuration on `/etc/samba/smb.conf` is required.

Enabling the share is immediate, however, disabling the share takes a few minutes to process.

This will only work when usershares are enabled on `/etc/samba/smb.conf`

    zfs share <<volume>>
    zfs unshare <<volume>>

# Properties

### Important setting

- Enable x(tended) attr(ibutes) (might be better to do this on vdev level if
  using ACL)

This is a performance setting, we want to enable extended attributes, but we
want the system attributes to be stored as extended atttributes too, that's
what SA stands for

`zfs set xattr=sa vdev/zvol`

- Set the dnode size

It is recommended in the manual to set this property to AUTO when using SAMBA

`zfs set dnodesize=auto vdev/zvol`

- Set a mount point for your ZVOL

Set where you want your device to sit

`zfs set mountpoint=/someplace vdev/zvol`

- Enable ACLs (recommended on vdev instead of zvol)

`zfs set aclinherit=passthrough vdev`
`zfs set acltype=posixacl vdev`
`zfs set xattr=sa vdev`


### Pool settings only available on creation time

- `ashift=12` - sets the sector size of the disks (for 4KiB / 4096 bytes)

Normally, disks report a smaller sector size for compatibility with the OS,
however, using the correct sector size for the pool will increase performance

### Volume settings only available on creation time

- `casesensitivity` which helps us when setting an SMB share for windows, setting:

`zfs create -o casesensitivity=mixed volume/fs`

### List ZFS FS properties

You can list all properties for all ZFS items (volumes and filesystems) with:

`zfs get all`

Or you can specify an item to examine:

`zfs get all vdev/zvol`

### Destroy everything

`zfs destroy vdev/zvol`
`zpool destroy vdev`

### Scrubbing

Start => `zpool scrub pool`
Stop => `zpool scrub -s pool`

# Procedures

## Migrating disks physically

1. `zpool export pool` for each pool in your disks
2. On the target system, once the disks have been moved `zpool import pool` for each pool from step 1
