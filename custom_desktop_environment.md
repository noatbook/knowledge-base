Removing a previous DE is recommended because of conflicting services

## Custom desktop environment

### Bare minimum components

- Window manager
  - i3
- Taskbar
  - polybar
- Terminal emulator
  - konsole
- File manager
  - dolphin
- Text editor
  - vim
  - nvim
  - kate

### Additional components

- Application launcher
  - dmenu
  - rofi
- Audio volume control
- Clipboard manager
- Desktop compositor
- Desktop wallpaper setter
- Display manager
- Display power saving settings
- Logout dialogue
- Mount tool
- Notification daemon
- Polkit authentication agent
- Screen locker
- Default application

### Call stack

- SDDM
  - X11 server
  - WM (plasma, i3)
    - Status bar (polybar)
    - Notification daemon (dunst)

## i3

Most processes interact directly with the WM and have to linked to it


