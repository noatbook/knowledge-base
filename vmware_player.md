### Copy virtual machines

1. Physically copy the folder containing the VM
2. Rename the folder
3. Edit the `.vmx` VM configuration file, change the `displayName` property to
   the new name of the virtual machine
4. Open the new VM from vmware player

### Add hard drives to VM

1. Turn off VM
2. Open settings
3. Press "Add" at the bottom and choose the correct device
