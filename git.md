# List remote URLS

    $ git remote -v

# Modify remote | Switch HTTPS and GIT modes

    $ git remote set-url origin <<url>>

