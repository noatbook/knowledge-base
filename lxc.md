# LXC (linux containers)

## Install dependencies

- lxc libvirt0 libpam-cgfs bridge-utils uidmap net-tools

## Unprivileged containers as root

You can create unprivileged containers as root to avoid additional steps to
setup the feature. The only requirement is to have a uid/gid mapping on the
host and a corresponding uid/gid mapping on the client so the right users can
exit the container if they need to, for example, for using system resources or
mounting filesystems.

## Configuration

### Network - [Source](https://ubuntu.com/blog/converting-eth0-to-br0-and-getting-all-your-lxc-or-lxd-onto-your-lan)

Add this to your `/etc/network/interfaces`, replace eth2 with the correct
interface

```
auto br0
iface br0 inet dhcp
  bridge-ifaces eth2
  bridge-ports eth2
  bridge_fd 0
  bridge_maxwait 0
  up ifconfig eth2 up

iface eth2 inet manual
```

### UID / GID mapping - [Source](https://linuxcontainers.org/lxc/getting-started/)

It might be necessary to map root in the container to root in the host, not
sure.

#### UID / GID mapping on host

Debian has already created a UID / GID map in `/etc/subuid` and `/etc/subgid`,
this means, each user and group in the host system already has 65535 different
UIDs or GIDs that map to the same user or group. This can be used to map lots of
different users on the container to a single user on the host.

#### UID / GID mapping on the container

You need to modify the default configuration file for lxc in
`/etc/lxc/default.conf` and enable it like this:

```
lxc.idmap = u 0 100000 65536
lxc.idmap = g 0 100000 65536
```

Making sure to use the correct offsets for the mapping desired. In the previous
example, the users 0-65535 are mapped to 100000-165535, this means even the root
user on the container is mapped to UID 100000, this may have to be tweaked for
some applications.

Proper isolation should be taken care of here, probably a new user for every
container, with just the permissions that this container will require

## Creation

```
lxc-create -n jackett -t debian -- -r buster
```

OR

```
lxc-create -o /tmp/lxc-create.log -l DEBUG -t download -n jackett -- -d debian -r buster -a amd64
```

## Starting



## Sources

- [Debian guide](https://wiki.debian.org/LXC)
- [LXC homepage](https://linuxcontainers.org)
- [Old LXC guides by creator](https://linuxcontainers.org/lxc/articles/)
