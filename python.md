### List all packages from a python installation

You can use `pip freeze` and redirect the output to a text file of your preference, like so:

`pip freeze > requirements.txt`

### Install packages from a text file

You can use the following command:

`pip install -r requirements.txt`

# Virtual environments

### Create virtual environment

You can use the following command:

`python -m venv /path/to/virtual/environment`

### Delete virtual environment

De-activate your virtual environment and delete the folder containing it
