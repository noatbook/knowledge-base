# Config file location

    /etc/samba/smb.conf

# Important options
## Global
- Disable usershares

    usershare path =
## User-defined shares
- Specify path to the shared folder

    path = <path>

- Turn share on

    available = yes

- Users that can connect to the share

    valid users = <users>

- Whether users can write to the share or not

    read only = no
    OR
    writeable = yes

- Controls if the share is visible or not

    browseable = yes
    OR
    browsable = yes

- No password is required to connect to the service

    public = yes
    OR
    guest ok = yes

- Default group for users connecting to the share

    group = <group>
    OR
    force group = <group>

- Everything inherits permissions from parent dir (might be better to just stick to ACLs)

    inherit permissions = yes

- Stick to UNIX ACLs (default no)

    inherit acls = yes

## Sample configuraton

```
[gringotts]
path = /gringotts
available = yes
valid users = frodo
writeable = yes
browseable = yes
public = no
group = shire
inherit permissions = yes
inherit acls = yes

[vault-712]
path = /vault-712
available = yes
valid users = frodo
writeable = yes
browseable = yes
public = no
group = shire
inherit permissions = yes
inherit acls = yes
```

# User management

### Adding a user

Use the command `smbpasswd -a user` to create a user called user
