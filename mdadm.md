# Print information about an array

`mdadm -D /dev/mdX`

OR

`mdadm --detail /dev/mdX`

# Stopping an array

1. Unmount

`umount /dev/mdX`

2. Stop the array(s)

- Stop all arrays

`mdadm --stop --scan`

- Stop a particular array

`mdadm --stop /dev/mdX`

# Deleting array

1. Stop array

2. Delete array

`mdadm --remove /dev/mdX`

3. Zero the superblock (probably similar to wipefs)

- Check the FS type of the disks that belong to the array

`lsblk --fs`

- Zero the superblocks

`mdadm --zero-superblock /dev/sdX...`

- Re-check

*In my experience the previous command did not work, so using wipefs*

- Wipe the FS flags with `wipefs`

*It also didn't work, so I tried the --force parameter on --zero-superblock*

`mdadm -v --zero-superblock --force /dev/sdX...`

*This did work*

4. Remove entries from `/etc/fstab`

5. Remove entries from `/etc/mdadm/mdadm.conf`

6. Remove array from early boot environment

`update-initramfs -u`
