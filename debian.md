### Debian installer - selecting options

You can select options with the spacebar

### Debian installer - error on `select and install packages`

- Check how the disks are partitioned (one vs multiple partitions)
- Drop into a terminal and check free space in devices with `df`
- Check free RAM too with `free -m`

The most probable cause for this would be that you have partitioned your disk
into multiple partitions for `/var`, `/tmp`, `/home` and
you allocated a low amount (<60GB) for the whole partitioning schema. When the
partitions are created, usually, the `/var` and `/tmp` directories are
small in comparison to the `/home` partition, therefore filling the space
before the installation is over.

#### Default partitioning schema

![](assets/2021-02-13.20_44_41-debian-install-default-partitions-guide-whole-disk-no-lvm.png)

The `/var` partition is usually used for logging, which we are making a lot of
during installation. The `/tmp` partition is where all items needed to be
downloaded or unpacked are stored, it fills up pretty fast when installing
things. Apparently the `/` partition can also take a lot of space, in general,
the guided setup probably only works on either complete disks or very
lightweight installations

#### Install error using guided partitioning and lvm - partitions are filling up

![install error, whole disk with lvm](assets/2021-02-13.19_16_13-debian-install-error-guide-lvm-whole-disk.png)

#### Install error using manual disk allocation and no lvm - partitions are filling up

![](assets/2021-02-14.01_07_26-debian-install-error-manual-disk-allocation.png)

#### Install error guided partitions no lvm - partitions are filling up

![](assets/2021-02-13.21_13_19-debian-install-errror-guided-whole-disk-no-lvm.png)

### Debian installar - optimal partition sizes with plasma DE

- `/` => 7GB
- `/var` => 3GB
- `swap` => 1GB
- `/tmp` => 1GB
- `/home`=> everything else

### Make all files/folders under a directory inherit the group of the parent

You need to set the `setgid` bit on the directory with:

`chmod g+s <dir>`

### Wipe device signatures | Erase filesystem, raid or partition table signature

It's probably a better idea to create a new partition table through `fdisk`
than to go through `dd` to erase the whole disk or through `wipefs`

Use `wipefs <device>` with no parameters to list all the signatures on the device.

Use `wipefs -a <device>` to erase all signatures from a device to partition it
like a blank disk

# User and permissions management

### Utilities

Use `adduser` as recommended by the manual instead of `useradd`
Use `addgroup` as recommended by the manual instead of `groupadd`
Use `deluser` as recommended by the manual instead of `userdel`
Use `delgroup` as recommended by the manual instead of `delgroup`

### Listing users and groups

Look in the files `/etc/passwd` for users and `/etc/groups` for groups

### Changing ownership of files and folders

chown -R user:group /folder

### Changing permissions of files and folders

chmod -R 770 /folder

# Setting an ACL

`setfacl -dRm u::rwX,g::rwX,o::0 /data`

# Daemons

# Systemd

`systemctl [--user] (enable|start|stop|status|restart)`

## User services

Sometimes you cannot create a system service to fullfil your needs because
there is a specific user-level dependency, in this case, user services are
created, these are just like the system services but run on ruser level, since
the user logs in until the last user session logs out

### Finding in which package dependencies are located

Reference: https://wiki.debian.org/apt-file

Install `apt-file` to search for a file in the existing packages.

After installation the database must be built with `apt-file update`

Then you can use `apt-file search <filename>`

Alternatively you can use `dpkg -S <filename>` however this command is
limited to packages already installed

### Configuration

#### Unattended upgrades

Install:

```
unattended-upgrades apt-listchanges
```

Configure:

```
/etc/apt/apt.conf.d/50unattended-upgrades
```

important configurations:

```
Unattended-Upgrade::Mail "root";
Unattended-Upgrade::Remove-Unused-Kernel-Packages "true";
Unattended-Upgrade::Remove-New-Unused-Dependencies "true";
Unattended-Upgrade::Remove-Unused-Dependencies "true";
```

